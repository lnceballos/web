export default function ({ $axios, $auth }, inject) {
  // Create a custom axios instance
  const api = $axios.create({
    headers: {
      common: {
        Accept: 'application/json'
      }
    }
  })

  // Set baseURL to something different
  api.setBaseURL(process.env.API_BASE_URL)

  // Inject to context as $api
  inject('api', api)
}
